package MonerochanBot;

import com.litesoftwares.coingecko.CoinGeckoApiClient;
import com.litesoftwares.coingecko.constant.Currency;
import com.litesoftwares.coingecko.impl.CoinGeckoApiClientImpl;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class Converter {

    private static final CoinGeckoApiClient client = new CoinGeckoApiClientImpl();

    private static final String XMR_FORMAT_EXCEPTION_REPLY = "Sorry Anon, I cannot parse that XMR value :(\n\nTry it like this:\n`/xmrusd 69.420420420420`";
    private static final String USD_FORMAT_EXCEPTION_REPLY = "Sorry Anon, I cannot parse that USD value :(\n\nTry it like this:\n`/usdxmr 420.69`";
    private static final String BTC_FORMAT_EXCEPTION_REPLY = "Sorry Anon, I cannot parse that BTC value :(\n\nTry it like this:\n`/btcxmr 0.42069`";
    private static final String MONERO = "monero";

    private static final int IS_LESS_THAN_ONE = 0;
    private static final int IS_LESS_THAN_TEN = 1;
    private static final int IS_LESS_THAN_THOUSAND = 2;

    private BigDecimal getXmrPriceInUsd() {
        return BigDecimal.valueOf(client.getPrice(MONERO, Currency.USD).get(MONERO).get(Currency.USD));
    }
    public String getXmrPriceInUsdAsString(){
         return formatUsdAmountAsString(getXmrPriceInUsd());
    }

    private BigDecimal getXmrPriceInBtc(){
        return BigDecimal.valueOf(client.getPrice(MONERO, Currency.BTC).get(MONERO).get(Currency.BTC));
    }
    public String getXmrPriceInBtcAsString(){
        return formatBtcAmountAsString(getXmrPriceInBtc());
    }

    public String convertXmrToUsd(String xmrAmountString) {
        try {
            return formatUsdAmountAsString(new BigDecimal(xmrAmountString).multiply(getXmrPriceInUsd()));
        }
        catch (NumberFormatException numberFormatException) {
            return XMR_FORMAT_EXCEPTION_REPLY;
        }
    }
    public String convertUsdToXmr(String usdAmountString) {
        try {
            return formatXmrAmountAsString(new BigDecimal((usdAmountString)).divide(getXmrPriceInUsd(), MathContext.DECIMAL128));
        }
        catch (NumberFormatException numberFormatException) {
            return USD_FORMAT_EXCEPTION_REPLY;
        }
    }

    public String convertBtcToXmr (String btcAmountString) {
        try {
            return formatXmrAmountAsString(new BigDecimal(btcAmountString).divide(getXmrPriceInBtc(), MathContext.DECIMAL128));
        }
        catch (NumberFormatException numberFormatException) {
            return BTC_FORMAT_EXCEPTION_REPLY;
        }
    }
    public String convertXmrToBtc (String xmrAmountString) {
        try {
            return formatBtcAmountAsString(new BigDecimal(xmrAmountString).multiply(getXmrPriceInBtc()));
        } catch (NumberFormatException numberFormatException) {
            return XMR_FORMAT_EXCEPTION_REPLY;
        }
    }

    private String formatXmrAmountAsString(BigDecimal xmrAmount) {
        boolean[] xmrAmountBoolean = amountBooleanArray(xmrAmount);

        if (xmrAmountBoolean[IS_LESS_THAN_ONE]) {
            return new DecimalFormat("#.############").format(xmrAmount) + " XMR";
        }
        else {
            if (xmrAmountBoolean[IS_LESS_THAN_TEN]) {
                return new DecimalFormat("#.###").format(xmrAmount) + " XMR";
            }
            else if (xmrAmountBoolean[IS_LESS_THAN_THOUSAND]) {
                return new DecimalFormat("#.##").format(xmrAmount) + " XMR";
            }
            else {
                return NumberFormat.getNumberInstance(Locale.US).format(xmrAmount) + " XMR";
            }
        }
    }
    private String formatUsdAmountAsString(BigDecimal usdAmount) {
        return NumberFormat.getCurrencyInstance(Locale.US).format(usdAmount);
    }
    private String formatBtcAmountAsString(BigDecimal btcAmount) {
        boolean[] btcAmountBoolean = amountBooleanArray(btcAmount);

        if (btcAmountBoolean[IS_LESS_THAN_ONE]) {
            return new DecimalFormat("#.############").format(btcAmount) + " BTC";
        }
        else {
            if (btcAmountBoolean[IS_LESS_THAN_TEN]) {
                return new DecimalFormat("#.#####").format(btcAmount) + " BTC";
            }
            else if (btcAmountBoolean[IS_LESS_THAN_THOUSAND]) {
                return new DecimalFormat("#.###").format(btcAmount) + " BTC";
            }
            else {
                return NumberFormat.getNumberInstance(Locale.US).format(btcAmount) + " BTC";
            }
        }
    }

    private boolean[] amountBooleanArray(BigDecimal amount) {
        boolean[] amountBooleanArray = {false, false, false};

        if (amount.compareTo(BigDecimal.ONE) <= 0) {
            amountBooleanArray[IS_LESS_THAN_ONE] = true;
        }
        else if (amount.compareTo(BigDecimal.TEN) < 0) {
            amountBooleanArray[IS_LESS_THAN_TEN] = true;
        }
        else if (amount.compareTo(BigDecimal.TEN) >= 0 && amount.compareTo(BigDecimal.TEN.multiply(BigDecimal.TEN).multiply(BigDecimal.TEN)) < 0) {
            amountBooleanArray[IS_LESS_THAN_THOUSAND] = true;
        }

        return amountBooleanArray;
    }
}
