package MonerochanBot;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;

import javax.annotation.Nullable;
import java.text.NumberFormat;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import static MonerochanBot.Artworks.*;
import static MonerochanBot.MonerochanBot.MONEROCHAN_BOT_NAME;

public class Responses {

    private static final String MONEROCHAN_IMG_URL = "https://i.redd.it/64drwdmwc1q61.png";
    private static final String MONERO_IMG_URL = "https://c.wallhere.com/photos/90/1c/Monero_cryptocurrency-1385995.jpg!d";
    private static final int MONERO_ORANGE = 0xF26722;
    private static final String EMBED_FOOTER = MONEROCHAN_BOT_NAME + " \uD83E\uDD0D";
    private static final String COINGECKO_LINK = "https://www.coingecko.com/en/coins/monero";

    private static Message createMessage(String msg) {
        return new MessageBuilder()
                .setContent(msg)
                .build();
    }
    private static MessageEmbed createMessageEmbed(@Nullable String embedTitle, @Nullable String embedTitleUrl,
                                                   @Nullable String description, @Nullable String imgUrl, int color) {
        EmbedBuilder embedBuilder = new EmbedBuilder();

        if (embedTitle != null && embedTitleUrl == null) {
            embedBuilder.setTitle(embedTitle);
        }
        if (embedTitle != null && embedTitleUrl != null) {
            embedBuilder.setTitle(embedTitle, embedTitleUrl);
        }
        if(description != null) {
            embedBuilder.setDescription(description);
        }
        if (imgUrl != null) {
            embedBuilder.setImage(imgUrl);
        }
        embedBuilder.setFooter(EMBED_FOOTER, MONEROCHAN_IMG_URL);
        embedBuilder.setColor(color);


        return embedBuilder.build();
    }

    public MessageEmbed serverJoinReply() {
        String embedTitle = "Monerochan: Putting the 'fun' in fungibility";
        String embedTitleUrl = "https://www.monerochan.art/commissions/irs_spanking.jpg";
        String description = "Hi Anons, it's Monerochan :3\n\nFor a full list of my commands, type:\n`/commands`";

        return createMessageEmbed(embedTitle, embedTitleUrl, description, MONEROCHAN_IMG_URL, MONERO_ORANGE);
    }
    public Message memberJoinReply(String memberName) {
        final String welcomeMsg = "Welcome " + memberName + " :3 I hope you have your boating licence ;)";
        return createMessage(welcomeMsg);
    }

    //Miscellaneous
    public Message hiReply(String authorName) {
        final String hiResponse = "Hi " + authorName + " :3";
        return createMessage(hiResponse);
    }
    public Message uwuReply() {
        final String uwuResponse = "uwu :3";
        return createMessage(uwuResponse);
    }
    public Message owoReply() {
        final String uwuResponse = "owo :3";
        return createMessage(uwuResponse);
    }
    public Message heartReply() {
        final String heartResponse = "<3";
        return createMessage(heartResponse);
    }
    public MessageEmbed richListReply() {
        String embedTitle = "Monero Rich List";
        String richList = "No 1.\n??? owns ??? XMR\n\nNo 2.\n??? owns ??? XMR\n\nNo 3.\n??? owns ??? XMR\n\nNo 4.\n??? owns ??? XMR\n\nNo 5.\n??? owns ??? XMR";

        return createMessageEmbed(embedTitle, null, richList, null, MONERO_ORANGE);
    }
    public MessageEmbed artReply() {
        String[] artInfo = new Artworks().getMonerochanArt();

        String artworkUrl = artInfo[ARTWORK_URL];
        String artistName = artInfo[ARTIST_NAME];
        String artistUrl = artInfo[ARTIST_URL];

        String description = "Artist: [" + artistName + "](" + artistUrl + ")";

        return createMessageEmbed(null, null, description, artworkUrl, MONERO_ORANGE);
    }

    //Help
    public Message helpReply() {
        final String helpResponse = "Hi Anon :) Try \"/commands\" for the full list of commands.";
        return createMessage(helpResponse);
    }
    public MessageEmbed commandsReply() {
        final String embedTitle = MONEROCHAN_BOT_NAME + " Commands";
        final String gettingStartedCommandsDescription = """
            **Getting Started**
           `/about` - Display a brief description of Monero.
           `/eli5` - Display an ELI5 description of Monero.
           `/glossary` - A glossary of important Monero phrases.
           `/bible` - Display the 'Mastering Monero' book.""";

        final String gettingXmrCommandsDescription = """
           **Getting XMR**
           `/getxmr` - Guide to getting some XMR.
           `/lm` - Guide to getting XMR anonymously on LocalMonero.
           `/mining` - Guide to mining some XMR.""";

        final String miscellaneousCommandsDescription = """ 
           **Miscellaneous**
           `/hi` - Say hi to Monerochan.
           `/art` - Display Monerochan art.
           `/richlist` - Display the top 5 XMR addresses.""";

        final String walletCommandsDescription = """
            **Monero Wallets**
            `/wallettypes` - Display a list of wallet types.
            `/gui` - Monero GUI Wallet Info
            `/cli` - Monero CLI Wallet Info
            `/cake` - Cake Wallet Info
            `/feather` - Feather Wallet Info
            `/monerujo` - Monerujo Wallet Info
            `/mymonero` - MyMonero Wallet Info
            `/paper` - Paper Wallet Guide
            `/hardware` Hardware Wallets Info""";
        final String conversionCommandsDescription = """
            **XMR Conversions**
            `/usdxmr` - USD -> XMR
            `/xmrusd` - XMR -> USD
            `/btcxmr` - BTC -> XMR
            `/xmrbtc` - XMR -> BTC""";

        final String donationsCommandDescription = """
                **Donations**
                `/donate` - Display the Monero General Fund donation address.
                `/donateart` - Display the Monerochan Art Fund donation address.
                `/donatedev` - Display my XMR donation address. Any donations will be greatly appreciated <3""";

        final String description = gettingStartedCommandsDescription + "\n\n" + gettingXmrCommandsDescription
                + "\n\n" + miscellaneousCommandsDescription + "\n\n" + walletCommandsDescription
                + "\n\n" + conversionCommandsDescription + "\n\n" + donationsCommandDescription;

        return createMessageEmbed(embedTitle, null, description, null, MONERO_ORANGE);
    }

    //Getting Started Responses
    public MessageEmbed aboutReply() {
        final String embedTitle = "About Monero";
        final String embedTitleUrl = "https://www.getmonero.org";
        final String embedMsg = """
            Monero (XMR) is a private, fungible & censorship-resistant cryptocurrency.

            "Monero" is an Esperanto word meaning coin or money.""";
        return createMessageEmbed(embedTitle, embedTitleUrl, embedMsg, MONERO_IMG_URL, MONERO_ORANGE);
    }
    public MessageEmbed eliFiveReply() {
        String embedTitle = "Monero ELI5 from Monero.how";
        String embedTitleUrl = "https://www.monero.how/monero-ELI5";
        String description = """
                Everyone stores their funds using a software application called a Monero wallet. Each wallet has its own receiving address. You tell other people this address so that they can send you funds.

                When someone sends you funds, you can’t tell who sent it to you (unless they want you to know). Similarly, when you send funds, the recipient won’t know it was you that sent it, unless you tell them it was you. Because the movements of funds are kept private, no one can tell how rich anyone else is.

                This is very different from Bitcoin, where everyone’s wealth and the people they’ve transacted with are a matter of public record. Monero’s privacy is important to prevent others from knowing how rich you are and to prevent them from spying on how you spend your money. It also keeps your business transactions confidential from competitors. Since Monero is untraceable, you do not have to worry that any funds you receive are tainted by anything suspicious the previous owner did with them.

                Monero has no central point of authority. When you send funds to someone, a worldwide network of computers will come to an agreement among themselves that ownership of the funds has passed from one anonymous person to another. This means Monero cannot be shut down by any one country or authority.

                The network of worldwide computers that verify and agree that transactions have taken place are called miners. The reason they are called miners is that they are rewarded with a small amount of funds in exchange for the work they do to verify transactions. The shared global record of transactions is called the blockchain.

                To get started with Monero, all you need to do is download the Monero wallet. Then buy some Monero using dollars, pounds or euros from an exchange. The funds will appear in your Monero wallet, and you will be able to send some of your funds to any other person's Monero wallet.\s""";

        return createMessageEmbed(embedTitle, embedTitleUrl, description, MONERO_IMG_URL, MONERO_ORANGE);
    }
    public MessageEmbed glossaryReply() {
        String embedTitle = "Important Monero phrases";
        String embedTitleUrl = "https://www.monero.how/monero-glossary#";
        return createMessageEmbed(embedTitle, embedTitleUrl, null, null, MONERO_ORANGE);
    }
    public MessageEmbed masteringMoneroReply() {
        String embedTitle = "Mastering Monero: The future of private transactions";
        String embedTitleUrl = "https://masteringmonero.com/book/Mastering%20Monero%20First%20Edition%20by%20SerHack%20and%20Monero%20Community.pdf";
        String description = """
                'Mastering Monero' is your guide through the world of Monero, a leading cryptocurrency with a focus on private and censorship-resistant transactions. This book contains everything you need to know to start using Monero in your business or day-to-day life, even if you have never understood or interacted with cryptocurrencies before.

                The first portion of the book is a friendly (non-technical) introductions to key concepts and skills - chapters 1 and 2 start from scratch, and cover everything you need to know to transact safely. For curious readers, chapters 3 and 4 conceptually explain how Monero's blockchain and privacy technologies work (no math). Later chapters dive deeply into the internals of Monero, its mathematics, its code, and how to contribute.""";
        String imgUrl = "https://masteringmonero.com/img/book-1-very-min.png";

        return createMessageEmbed(embedTitle, embedTitleUrl, description, imgUrl, MONERO_ORANGE);
    }

    //Getting XMR Responses
    public Collection<? extends MessageEmbed> getXmrGuideReply() {
        return List.of(new MessageEmbed[]{moneroHowGuide(), localMoneroGuide()});
    }
    public MessageEmbed moneroHowGuide() {
        String embedTitle = "Buying XMR";
        String embedTitleUrl = "https://www.monero.how/how-to-buy-monero";
        String description = "A guide on how to buy XMR. Once done, withdraw your XMR to your own wallet ASAP.\n\nAlways remember:\n> Not your keys, not your XMR.";

        return createMessageEmbed(embedTitle, embedTitleUrl, description, MONERO_IMG_URL, MONERO_ORANGE);
    }
    public MessageEmbed localMoneroGuide() {
        String embedTitle = "Buying XMR anonymously with LocalMonero";
        String embedTitleUrl = "https://localmonero.co";
        String description = """
                Clearnet
                https://localmonero.co

                Onion (Tor required):
                http://nehdddktmhvqklsnkjqcbpmb63htee2iznpcbs5tgzctipxykpj6yrid.onion/

                Eepsite (I2P required)
                http://yeyar743vuwmm6fpgf3x6bzmj7fxb5uxhuoxx4ea76wqssdi4f3q.b32.i2p/""";


        return createMessageEmbed(embedTitle, embedTitleUrl, description, null, MONERO_ORANGE);
    }

    //Mining XMR Responses
    public Collection<? extends MessageEmbed> getMiningGuideReply() {
        return List.of(new MessageEmbed[]{soloMiningGuide(), p2PoolMiningGuide()});
    }
    public MessageEmbed soloMiningGuide() {
        String[] monerochanMiningInfo = new Artworks().getMonerochanMining();
        String embedTitle = "How to Solo Mine XMR (GUI)";
        String embedTitleUrl = "https://www.getmonero.org/resources/user-guides/solo_mine_GUI.html";
        String imgUrl = monerochanMiningInfo[ARTWORK_URL];
        String artistName = monerochanMiningInfo[ARTIST_NAME];
        String artistUrl = monerochanMiningInfo[ARTIST_URL];
        String description = "Artist: [" + artistName + "]" + "(" + artistUrl + ")";

        return createMessageEmbed(embedTitle, embedTitleUrl, description, imgUrl, MONERO_ORANGE);
    }
    public MessageEmbed p2PoolMiningGuide() {
        String embedTitle = "Decentralised Pool Mining with P2Pool";
        String embedTitleUrl = "https://gitlab.com/Winston69/discord-monerochan-bot/-/blob/master/guides/p2pool.md";
        String description = "Monero P2Pool is a peer-to-peer Monero mining pool developed from scratch by SChernykh (also known as sech1). " +
                "P2Pool combines the advantages of pool and solo mining; you still fully control your Monero node and what it mines, but you get frequent payouts like on a regular pool.";
        return createMessageEmbed(embedTitle, embedTitleUrl, description, null, MONERO_ORANGE);
    }

    //Wallets Responses
    public Collection<? extends MessageEmbed> allWalletsReply() {
        return List.of(new MessageEmbed[]{guiWalletReply(), cliWalletReply(), cakeWalletReply(),
                featherWalletReply(), monerujoReply(), myMoneroWalletReply()});
    }
    public MessageEmbed walletTypesReply() {
        final String embedTitle = "Monero Wallet Types";
        final String softwareWalletDefinition = "Software Wallet:\nA piece of software used to send, " +
                "receive & manage XMR. Web wallets, desktop wallets, mobile wallets, " +
                "GUI & CLI wallets are all examples of software wallets.";
        final String hardwareWalletDefinition = "Hardware Wallet:\nA physical device that encrypts and stores your private keys completely offline. The device must be connected to a computer to access the funds.";
        final String paperWalletDefinition = "Paper Wallet:\nA piece of paper that contains your public and private keys, usually in plain text or as a QR code.";
        final String hotWalletDefinition = "Hot Wallet:\nA wallet that is connected to the internet e.g. web, desktop & mobile wallets.";
        final String coldWalletDefinition = "Cold Wallet:\nA wallet that has no connection to the internet e.g. hardware or paper wallets.";
        final String description = softwareWalletDefinition + "\n\n" +
                hardwareWalletDefinition + "\n\n" + paperWalletDefinition + "\n\n" + hotWalletDefinition +
                "\n\n" + coldWalletDefinition;

        return createMessageEmbed(embedTitle, null, description, null, MONERO_ORANGE);
    }
    public MessageEmbed guiWalletReply() {
        final String embedTitle = "Monero GUI Wallet";
        final String embedTitleUrl = "https://www.getmonero.org/downloads/#gui";
        final String description = "A free & open-source graphical user interface (GUI) wallet " +
                "developed by the Monero community, suitable for beginners and advanced users alike.";
        final String downloadsHyperlink = "[GUI Wallet Downloads](https://www.getmonero.org/downloads/#gui)";
        final String guideHyperlink = "[GUI Wallet Guide](https://github.com/monero-ecosystem/monero-GUI-guide/blob/master/monero-GUI-guide.md)";
        final String sourceHyperlink = "[Source Code](https://github.com/monero-project/monero-gui)";
        final String embedMsg = description + "\n\n" + downloadsHyperlink + "\n\n" + guideHyperlink + "\n\n" + sourceHyperlink;
        final String imgUrl = "https://www.getmonero.org/img/downloads/gui.png";

        return createMessageEmbed(embedTitle, embedTitleUrl, embedMsg, imgUrl, MONERO_ORANGE);
    }
    public MessageEmbed cliWalletReply() {
        final String embedTitle = "Monero CLI Wallet";
        final String embedTitleUrl = "https://www.getmonero.org/downloads/#cli";
        final String description = "A free & open-source command line interface (CLI) wallet " +
                "developed by the Monero community, best suited to experienced Monero users and developers.";
        final String downloadHyperlink = "[CLI Wallet Downloads](https://www.getmonero.org/downloads/#cli)";
        final String guideHyperlink = "[CLI Wallet Guide](https://www.getmonero.org/resources/user-guides/monero-wallet-cli.html)";
        final String sourceHyperlink = "[Source Code](https://github.com/monero-project/monero)";
        final String embedMsg = description + "\n\n" + downloadHyperlink + "\n\n" + guideHyperlink + "\n\n" + sourceHyperlink;
        final String imgUrl = "https://www.getmonero.org/img/downloads/cli.png";

        return createMessageEmbed(embedTitle, embedTitleUrl, embedMsg, imgUrl, MONERO_ORANGE);
    }
    public MessageEmbed cakeWalletReply() {
        final String embedTitle = "Cake Wallet";
        final String embedTitleUrl = "https://cakewallet.com";
        final String description = "A lightweight wallet for Android and iOS users which also supports Bitcoin & Litecoin.";
        final String googlePlayStoreHyperlink = "[Google Play Store](https://play.google.com/store/apps/details?id=com.cakewallet.cake_wallet)";
        final String iosAppStoreHyperlink = "[iOS App Store](https://apps.apple.com/us/app/cake-wallet-for-xmr-monero/id1334702542)";
        final String apkHyperlink = "[Android APK](https://github.com/cake-tech/cake_wallet/releases)";
        final String downloads = googlePlayStoreHyperlink + "\n\n" + iosAppStoreHyperlink + "\n\n" + apkHyperlink;
        final String guideHyperlink = "[Cake Wallet Guide](https://cakewallet.com/guide)";
        final String sourceHyperlink = "[Source Code](https://github.com/cake-tech/cake_wallet)";
        final String embedMsg = description + "\n\n" + downloads + "\n\n" + guideHyperlink + "\n\n" + sourceHyperlink;
        final String imgUrl = "https://cakewallet.com/assets/images/about/img1.png";

        return createMessageEmbed(embedTitle, embedTitleUrl, embedMsg, imgUrl, MONERO_ORANGE);
    }
    public MessageEmbed featherWalletReply() {
        final String embedTitle = "Feather Wallet";
        final String embedTitleUrl = "https://featherwallet.org";
        final String description = "Feather Wallet is a lightweight desktop Monero wallet " +
                "with an extensive set of features & settings. It is great for both beginners and experienced users.";
        final String downloadHyperlink = "[Feather Wallet Downloads](https://featherwallet.org/download)";
        final String guideHyperlink = "[Feather Wallet Docs](https://docs.featherwallet.org)";
        final String sourceHyperlink = "[Source Code](https://git.featherwallet.org/feather/feather)";
        final String embedMsg = description + "\n\n" + downloadHyperlink + "\n\n" + guideHyperlink + "\n\n" + sourceHyperlink;
        final String imgUrl = "https://featherwallet.org/theme/img/home_monero_light.png";

        return createMessageEmbed(embedTitle, embedTitleUrl, embedMsg, imgUrl, MONERO_ORANGE);
    }
    public MessageEmbed monerujoReply() {
        final String embedTitle = "Monerujo Wallet";
        final String embedTitleUrl = "https://monerujo.io/";
        final String description = "Monerujo is a lightweight wallet for Android only.";
        final String googlePlayStoreHyperlink = "[Google Play Store](https://play.google.com/store/apps/details?id=com.m2049r.xmrwallet)";
        final String fdroidHyperlink = "[F-Droid](https://play.google.com/store/apps/details?id=com.m2049r.xmrwallet)";
        final String guideHyperlink = "[Monerujo Docs](https://github.com/m2049r/xmrwallet/blob/master/doc/FAQ.md)";
        final String sourceHyperlink = "[Source Code](https://github.com/m2049r/xmrwallet)";
        final String downloads = googlePlayStoreHyperlink + "\n\n" + fdroidHyperlink;
        final String embedMsg = description + "\n\n" + downloads +
                "\n\n" + guideHyperlink + "\n\n" + sourceHyperlink;
        final String imgUrl = "https://www.monerujo.io/img/monerujo_logo.png";

        return createMessageEmbed(embedTitle, embedTitleUrl, embedMsg, imgUrl, MONERO_ORANGE);
    }
    public MessageEmbed myMoneroWalletReply() {
        final String embedTitle = "MyMonero Wallet";
        final String embedTitleUrl = "https://mymonero.com";
        final String description = "MyMonero is a lightweight wallet which is available on many platforms including web browsers. It features an intuitive and user-friendly UI, making it an excellent choice for beginners.";
        final String webWalletHyperlink = "[MyMonero Web Wallet](https://wallet.mymonero.com)";
        final String desktopDownloadsHyperlink = "[MyMonero Desktop Downloads](https://github.com/mymonero/mymonero-app-js/releases)";
        final String googlePlayStoreHyperlink = "[Google Play Store](https://play.google.com/store/apps/details?id=com.mymonero.official_android_application)";
        final String iosAppStoreHyperlink = "[iOS App Store](https://apps.apple.com/us/app/apple-store/id1372508199)";
        final String guideHyperlink = "[Guide](https://wallets.com/mymonero-review/#setup-guide)";
        final String sourceHyperlink = "[Source](https://github.com/mymonero)";
        final String downloads = webWalletHyperlink + "\n\n" + desktopDownloadsHyperlink +
                "\n\n" + googlePlayStoreHyperlink + "\n\n" + iosAppStoreHyperlink;
        final String embedMsg = description + "\n\n" +  downloads + "\n\n" + guideHyperlink + "\n\n" + sourceHyperlink;
        final String imgUrl = "https://mymonero.com/assets/images/screens/3.png";

        return createMessageEmbed(embedTitle, embedTitleUrl, embedMsg, imgUrl, MONERO_ORANGE);
    }
    public MessageEmbed sendPaperWalletGuideReply() {
        final String embedTitle = "Create a Paper Wallet using Tails (Advanced)";
        final String embedTitleUrl = "https://gitlab.com/Winston69/discord-monerochan-bot/-/blob/master/guides/paperwalletguide.md";
        final String imgUrl = "https://coindoo.com/wp-content/uploads/2018/11/monero-paper.jpg";

        return createMessageEmbed(embedTitle, embedTitleUrl, null, imgUrl, MONERO_ORANGE);
    }
    public Collection<? extends MessageEmbed> sendHardwareWalletsReply() {
        return List.of(new MessageEmbed[]{ledgerWalletReply(), trezorWalletReply()});
    }
    public MessageEmbed ledgerWalletReply() {
        String embedTitle = "Ledger Wallet";
        String embedTitleUrl = "https://www.ledger.com";
        String imgUrl = "https://www.ledger.com/wp-content/uploads/2021/11/LNS-home.png";

        return createMessageEmbed(embedTitle, embedTitleUrl, null, imgUrl, MONERO_ORANGE);
    }
    public MessageEmbed trezorWalletReply() {
        String embedTitle = "Trezor Model T Wallet";
        String embedTitleUrl = "https://trezor.io/";
        String description = "**Warning:** ONLY the Trezor Model T hardware wallet supports XMR.";
        String imgUrl = "https://trezor.io/static/images/perspective.webp";

        return createMessageEmbed(embedTitle, embedTitleUrl, description, imgUrl, MONERO_ORANGE);
    }

    //Conversion Responses
    public MessageEmbed getPriceReply() {
        Converter converter = new Converter();

        String embedTitle = "XMR Prices";
        String description = "1 XMR = " + converter.getXmrPriceInUsdAsString() + " or " + converter.getXmrPriceInBtcAsString();

        return createMessageEmbed(embedTitle, COINGECKO_LINK, description, null, MONERO_ORANGE);
    }
    public MessageEmbed xmrToUsdConversionReply(String xmrAmount) {
        final String embedTitle = "XMR to USD Conversion";
        String description;
        try {
            description = xmrAmount + " XMR is currently worth around " + new Converter().convertXmrToUsd(xmrAmount);
        }
        catch (NumberFormatException numberFormatException) {
            description = "Sorry Anon, I need an amount of XMR to convert :(\n\nTry it like this:\n`/xmrusd 69.420";
        }
        return createMessageEmbed(embedTitle, COINGECKO_LINK, description, null, MONERO_ORANGE);
    }
    public MessageEmbed usdToXmrConversionReply(String usdAmount) {
        final String embedTitle = "USD to XMR Conversion";
        String description;
        try {
            description = NumberFormat.getCurrencyInstance(Locale.US).format(Double.parseDouble(usdAmount)) + " is currently worth around " + new Converter().convertUsdToXmr(usdAmount);
        }
        catch (NumberFormatException numberFormatException) {
            description = "Sorry Anon, I need an amount of USD to convert :(\n\nTry it like this:\n`/usdxmr 420.69`";
        }
        return createMessageEmbed(embedTitle, COINGECKO_LINK, description, null, MONERO_ORANGE);

    }
    public MessageEmbed xmrToBtcConversionReply(String xmrAmount) {
        final String embedTitle = "XMR to BTC Conversion";
        String description;
        try {
            description = xmrAmount + " XMR is currently worth around " + new Converter().convertXmrToBtc(xmrAmount);
        }
        catch (NumberFormatException numberFormatException) {
            description = "Sorry Anon, I need an amount of XMR to convert :(\n\nTry it like this:\n`/xmrbtc 69.420`";
        }
        return createMessageEmbed(embedTitle, COINGECKO_LINK, description, null, MONERO_ORANGE);
    }
    public MessageEmbed btcToXmrConversionReply(String btcAmount) {
        final String embedTitle = "BTC to XMR Conversion";
        String description;
        try {
            description = btcAmount + " BTC is currently worth around "
                    + new Converter().convertBtcToXmr(btcAmount);
        }
        catch (NumberFormatException numberFormatException) {
            description = "Sorry Anon, I need an amount of BTC to convert :(\n\nTry it like this:\n`/btcxmr 4.2069`";
        }
        return createMessageEmbed(embedTitle, COINGECKO_LINK, description, null, MONERO_ORANGE);
    }

    //Error Responses
    public Message unknownCommandReply() {
        String unknownCommandReply = "Sorry Anon, I don't recognise that command :(";
        return new MessageBuilder()
                .setContent(unknownCommandReply)
                .build();
    }
    public Message mustBePrivateMessageReply() {
        final String mustBePrivateMsgReply = "Sorry Anon, but you can only use that command with me in private ;)";
        return new MessageBuilder()
                .setContent(mustBePrivateMsgReply)
                .build();
    }

    public MessageEmbed donateGeneralReply() {
        String embedTitle = "Monero General Fund";
        String embedTitleUrl = "https://ccs.getmonero.org/";
        String description = "Address: 888tNkZrPN6JsEgekjMnABU4TBzc2Dt29EPAvkRxbANsAnjyPbb3iQ1YBRk1UXcdRsiKc9dhwMVgN5S9cQUiyoogDavup3H";
        String imgUrl = "https://ccs.getmonero.org/img/donate-monero.png";

        return createMessageEmbed(embedTitle, embedTitleUrl, description, imgUrl, MONERO_ORANGE);
    }
    public MessageEmbed donateArtReply() {
        String embedTitle = "Monerochan Art Fund";
        String embedTitleUrl = "https://www.monerochan.art/";
        String description = "Address: 43YSiaRs1TuDQa4XrBQz46ZNFH5bc1kgUXoLDLvZ5VSH8Ubtqh2AQP7fz2YD173cxNBkk3nYJbGGDKLdDbnbtUWYDhy6hTu";
        String imgUrl = "https://gitlab.com/Winston69/discord-monerochan-bot/-/raw/master/img/monerochanartfundqrcode.png";

        return createMessageEmbed(embedTitle, embedTitleUrl, description, imgUrl, MONERO_ORANGE);
    }
    public MessageEmbed donateDevReply() {
        String embedTitle = "Donate to the Monerochan Bot Developer";
        String embedTitleUrl = "https://gitlab.com/Winston69";
        String description = "Address: 861xXfb2i6dQFjGD8h4GiL4Nu3stgUJxfXegvVuCyYHqP9gfY1PBwwN3td3MQgHzXKR9Eh95dDbx2Ema46fkA9Ya61qDefZ\n\n All donations are greatly appreciated as I am broke as fug.";
        String thumbnailUrl = "https://static.tvtropes.org/pmwiki/pub/images/basement_5691.png";
        String imgUrl = "https://gitlab.com/Winston69/discord-monerochan-bot/-/raw/master/img/mydonationqrcode.png";
        return new EmbedBuilder()
                .setTitle(embedTitle, embedTitleUrl)
                .setDescription(description)
                .setImage(imgUrl)
                .setThumbnail(thumbnailUrl)
                .setFooter("From the developer who hasn't seen the sun in 4 years", thumbnailUrl)
                .setColor(MONERO_ORANGE)
                .build();
    }
}